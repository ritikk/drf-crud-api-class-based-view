import json

import requests

URL = "http://127.0.0.1:8000/studentapi/"


def get_data(id=None):
    data = {}
    if id is not None:
        data['id'] = id
    json_data = json.dumps(data)
    r = requests.get(url=URL, data=json_data)
    data = r.json()  # Extracting the data that came back in r
    print(data)


# get_data()


def create_data():
    data = {
        'name': 'Rushabh Shah',
        'roll': 4,
        'city': 'Rajkot'
    }
    json_dta = json.dumps(data)
    r = requests.post(url=URL, data=json_dta)
    data = r.json()
    print(data)


# create_data()


def update_data():
    data = {
        'id': 5,  # To update we must write the id that we want to update
        'name': 'Nimesh Dulam',
        'city': 'Somewhere in Andhra Pradesh'
    }

    json_data = json.dumps(data)
    r = requests.put(url=URL, data=json_data)
    data = r.json()
    print(data)


# update_data()


def delete_data():
    data = {
        'id': 5
    }

    json_data = json.dumps(data)
    r = requests.delete(url=URL, data=json_data)
    data = r.json()
    print(data)


# delete_data()
